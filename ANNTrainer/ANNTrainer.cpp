#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;


	vector<vector<float>>input, output;
	vector<int> config;
	config = { 2, 10, 10, 1 };
	ANN::LoadData("../learn.txt", input, output);

	auto ann = ANN::CreateNeuralNetwork(config);
	ann->MakeTrain(input, output, 100000);
	ann->Save("../data1.txt");
	system("pause");
	return 0;
}