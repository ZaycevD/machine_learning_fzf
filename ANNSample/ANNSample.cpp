#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	vector<vector<float>>input, output;
	cout << GetTestString().c_str() << endl;

	ANN::LoadData("../learn.txt", input, output);
	vector<int> config;
	config = { 2, 10, 10, 1 };

	auto ann = ANN::CreateNeuralNetwork(config);
	ann->Load("../data1.txt");

	//ANN::NeuralNetwork::GetType();

	//ANN::LoadData("../data.txt", input, output);
	for (int i = 0; i < input.size(); i++)
	{
		auto out1 = ann->Predict(input[i]);
		for (int j = 0; j < out1.size(); j++)
		{
			cout << out1[j] << " ";
		}

		cout << endl;
		for (int j = 0; j < out1.size(); j++)
		{
			cout << output[i][j] << " ";
		}
		cout << endl;
	}


	system("pause");
	return 0;
}